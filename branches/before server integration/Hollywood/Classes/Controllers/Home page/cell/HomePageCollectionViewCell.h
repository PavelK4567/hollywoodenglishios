//
//  HomePageCollectionViewCell.h
//  Hollywood
//
//  Created by Kiril Kiroski on 8/8/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomePageCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UILabel *labelLessonInfo;
@property (nonatomic, weak) IBOutlet UILabel *labelLessonNumber;
@property (nonatomic, weak) IBOutlet UIImageView *lessonStatusImageView;
@property (nonatomic, weak) IBOutlet UIImageView *lessonThumbnailImageView;

@property (nonatomic, weak) IBOutlet UIImageView *cellBg;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *constraintStatusVsThumb;
@property (nonatomic) BOOL fromLessonSummary;

- (void)setLessonData:(id)lessonData;

@end

//
//  LessonViewController.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/24/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewVehicle.h"
#import "SwipeKircaVehicle.h"
#import "SwipeCarouselVehicle.h"
#import "KSVideoPlayerView.h"
#import "LessonProgressView.h"
#import "VoiceRecoginitionVehicle.h"
#import "CacheManager.h"
#import "LMBaseViewController.h"


@interface LessonViewController : LMBaseViewController <CacheManagerDelegate, UIScrollViewDelegate, BaseViewVehicleDelegate,playerViewDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, assign) NSInteger lessonNumber;
@property (nonatomic, assign) BOOL continueSession;
@property (nonatomic, assign) BOOL isLoadingFinished;
@property (nonatomic, assign) BOOL isPopupOpened;
@property (nonatomic, assign) BOOL videoHihgQuality;

@property (weak, nonatomic) IBOutlet UILabel *execiseChoiseLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@end

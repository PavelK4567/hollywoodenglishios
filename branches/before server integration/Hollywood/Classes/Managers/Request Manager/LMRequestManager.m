//
//  LMRequestManager.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/21/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LMRequestManager.h"

@implementation LMRequestManager

SINGLETON_GCD(LMRequestManager)

- (void)getDataFor:(ELMRequestType)type
           headers:(NSDictionary *)headers
withSuccessCallBack:(void (^)(NSDictionary *responseObject)) successCallback
andWithErrorCallBack:(void (^)(NSString *errorMessage)) errorCallback
{
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSMutableURLRequest *req = [self post:type headers:headers];
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (!error) {
            NSLog(@"Reply JSON: %@", responseObject);
            successCallback ((NSDictionary *)responseObject);
        } else {
            errorCallback([error localizedDescription]);
        }
    }] resume];
}

- (NSMutableURLRequest *)post:(ELMRequestType)type headers:(NSDictionary *)headers{
    NSString *urlString = [self urlForType:type headers:headers];
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlString parameters:nil error:nil];
    
    request.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    return request;
}


- (NSString *)urlForType:(ELMRequestType)type headers:(NSDictionary *)headers {
    NSString *path = 0;
    switch (type) {
            
        case kRequestFirstLaunchAPICall:
            self.groupID = @"1";
            self.contentID = @"1";
            path = @"http://hollywood.la-mark-il.com:8080/hollywoodenglish/api/firstLaunch?courseId=62&userId=45&mode=stage";
            //path = [NSString stringWithFormat:@"%@/sendLoginToken?msisdn=%@&groupID=%@&contentID=%@", kApiBaseUrl, headers[kUserMSISDN],self.groupID,self.contentID];
            break;
        
        default:
            break;
    }
    if (path) {
        return path;
    }
    return 0;
}

@end

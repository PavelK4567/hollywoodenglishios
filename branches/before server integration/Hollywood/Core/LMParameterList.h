//
//  LMParameterList.h
//  Hollywood
//
//  Created by Kiril Kiroski on 7/20/16.
//  Copyright © 2016 aa. All rights reserved.
//


//#ifndef EnglishCourse_LMParameterList_h
//#define EnglishCourse_LMParameterList_h

//Scoring
//Correct answer	Param-001
//Incorrect answer	Param-002

#define kParam001 100
#define kParam002 20
#define kParam003 19
#define kParam004 50
#define kParam005 89
//The user will be assigned with Param-014 VIP Passes
//Param-007 points, the application will raise the VIP Passes count by Param-015.
#define kParam014 7
#define kParam007 150
#define kParam015 5

#define kParam022 1
#define kParam023 5
#define kParam024 7
#define kParam025 9
#define kParam026 10
#define kParam027 10

#define kParam012 2
#define kParam019 2
//#endif

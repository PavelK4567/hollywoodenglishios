//
//  CourseObject+CoreDataClass.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class LevelObject;

NS_ASSUME_NONNULL_BEGIN

@interface CourseObject : NSManagedObject

- (void) setData:(NSDictionary*)dataDictionary;
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END

#import "CourseObject+CoreDataProperties.h"

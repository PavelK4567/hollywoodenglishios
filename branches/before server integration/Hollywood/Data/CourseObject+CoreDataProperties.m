//
//  CourseObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/23/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "CourseObject+CoreDataProperties.h"

@implementation CourseObject (CoreDataProperties)

+ (NSFetchRequest<CourseObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"CourseObject"];
}

@dynamic course_description;
@dynamic course_id;
@dynamic course_name;
@dynamic course_source_language;
@dynamic course_levels;

@end

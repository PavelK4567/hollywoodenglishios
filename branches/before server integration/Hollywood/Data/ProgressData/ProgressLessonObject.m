//
//  ProgressLessonObject.m
//  Hollywood
//
//  Created by Kiril Kiroski on 8/10/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "ProgressLessonObject.h"
#import "ProgressVehicleObject.h"

@implementation ProgressLessonObject

- (void) setData:(NSDictionary*)dataDictionary{
    
}

- (void) setInitialData{
    self.maxVehicle = 0;
    self.currentVehicle = 0;
    self.scoreForLesson = 0;
    self.starsNumber = 0;
    for (int i= 0; i < 50; i++) {
        ProgressVehicleObject *newObject = [ProgressVehicleObject MR_createEntity];
        newObject.vehicleOrder = i;
        [newObject setInitialData];
        [self addVehicleProgresObject:newObject];
    }

    
}

- (ProgressVehicleObject*)getCurrentProgressVehicleObject{
    
    NSSet *set2 = self.vehicleProgres ;
    NSArray *data2Array = [set2 allObjects];
    ////sorting
    NSSortDescriptor *sortDescriptor2;
    sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"vehicleOrder"
                                                  ascending:YES];
    NSArray *sortDescriptors2 = [NSArray arrayWithObject:sortDescriptor2];
    NSArray *sorted2Array = [data2Array sortedArrayUsingDescriptors:sortDescriptors2];
    /////
    ProgressVehicleObject *tempProgressVehicle = [sorted2Array objectAtIndex:self.currentVehicle];
    return tempProgressVehicle;
    
}

@end

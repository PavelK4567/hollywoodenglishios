//
//  ProgressVehicleObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 8/11/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ProgressVehicleObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProgressVehicleObject (CoreDataProperties)

@property (nonatomic) BOOL achived;
@property (nonatomic) int32_t progres;
@property (nonatomic) int32_t vehicleOrder;

@end

NS_ASSUME_NONNULL_END

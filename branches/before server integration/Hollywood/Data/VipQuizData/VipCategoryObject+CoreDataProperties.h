//
//  VipCategoryObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 10/4/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "VipCategoryObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface VipCategoryObject (CoreDataProperties)

+ (NSFetchRequest<VipCategoryObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *categoryId;
@property (nullable, nonatomic, copy) NSString *categoryName;
@property (nullable, nonatomic, copy) NSNumber *displayOrder;
@property (nullable, nonatomic, copy) NSString *imageName;
@property (nullable, nonatomic, copy) NSString *imageUrl;
@property (nullable, nonatomic, copy) NSNumber *availability;
@property (nullable, nonatomic, retain) NSSet<VipQuizObject *> *firstQuizInstanceList;
@property (nullable, nonatomic, retain) NSSet<VipQuizObject *> *quizList;

@end

@interface VipCategoryObject (CoreDataGeneratedAccessors)

- (void)addFirstQuizInstanceListObject:(VipQuizObject *)value;
- (void)removeFirstQuizInstanceListObject:(VipQuizObject *)value;
- (void)addFirstQuizInstanceList:(NSSet<VipQuizObject *> *)values;
- (void)removeFirstQuizInstanceList:(NSSet<VipQuizObject *> *)values;

- (void)addQuizListObject:(VipQuizObject *)value;
- (void)removeQuizListObject:(VipQuizObject *)value;
- (void)addQuizList:(NSSet<VipQuizObject *> *)values;
- (void)removeQuizList:(NSSet<VipQuizObject *> *)values;

@end

NS_ASSUME_NONNULL_END

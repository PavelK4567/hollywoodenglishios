//
//  LessonObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "LessonObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface LessonObject (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *initial;
@property (nullable, nonatomic, retain) NSString *lesson_description;
@property (nullable, nonatomic, retain) NSString *lesson_home_page_image;
@property (nullable, nonatomic, retain) NSNumber *lesson_id;
@property (nullable, nonatomic, retain) NSString *lesson_name;
@property (nullable, nonatomic, retain) NSString *lesson_teaser;
@property (nullable, nonatomic, retain) NSSet<BaseVehiclesObject *> *lesson_vehicles;

@end

@interface LessonObject (CoreDataGeneratedAccessors)

- (void)addLesson_vehiclesObject:(BaseVehiclesObject *)value;
- (void)removeLesson_vehiclesObject:(BaseVehiclesObject *)value;
- (void)addLesson_vehicles:(NSSet<BaseVehiclesObject *> *)values;
- (void)removeLesson_vehicles:(NSSet<BaseVehiclesObject *> *)values;

@end

NS_ASSUME_NONNULL_END

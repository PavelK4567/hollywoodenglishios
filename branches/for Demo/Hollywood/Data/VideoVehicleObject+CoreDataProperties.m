//
//  VideoVehicleObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "VideoVehicleObject+CoreDataProperties.h"

@implementation VideoVehicleObject (CoreDataProperties)

@dynamic text;
@dynamic video;
@dynamic videoPreview;

@end

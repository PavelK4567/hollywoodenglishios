//
//  VideoVehicleObject.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/28/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseVehiclesObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface VideoVehicleObject : BaseVehiclesObject

- (void) setData:(NSDictionary*)dataDictionary;

@end

NS_ASSUME_NONNULL_END

#import "VideoVehicleObject+CoreDataProperties.h"

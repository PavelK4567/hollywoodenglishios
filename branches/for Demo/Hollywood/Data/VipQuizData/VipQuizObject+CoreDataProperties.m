//
//  VipQuizObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 10/28/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "VipQuizObject+CoreDataProperties.h"

@implementation VipQuizObject (CoreDataProperties)

+ (NSFetchRequest<VipQuizObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"VipQuizObject"];
}

@dynamic availability;
@dynamic avEndDate;
@dynamic avStartDate;
@dynamic imageName;
@dynamic imageUrl;
@dynamic isPromo;
@dynamic orderInCategory;
@dynamic price;
@dynamic quizId;
@dynamic quizListDescription;
@dynamic quizName;
@dynamic teaser;
@dynamic instanceList;

@end

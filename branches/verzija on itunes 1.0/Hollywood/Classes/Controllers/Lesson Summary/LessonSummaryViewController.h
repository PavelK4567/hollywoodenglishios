//
//  LessonSummaryViewController.h
//  Hollywood
//
//  Created by Kiril Kiroski on 5/22/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LMBaseViewController.h"
#import "ProgressLessonObject.h"
#import "AchievementsObject+CoreDataClass.h"

@interface LessonSummaryViewController : LMBaseViewController<UITableViewDelegate,UITableViewDataSource, CacheManagerDelegate>

@property(nonatomic) float vehiclePassCorrect;
@property(nonatomic) float vehicleNumber;
@property(nonatomic,retain) LessonObject *currentLessonObject;

@end

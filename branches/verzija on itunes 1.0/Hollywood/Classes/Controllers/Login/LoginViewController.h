//
//  LoginViewController.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/7/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LMBaseViewController.h"
#import "HomePageCollectionViewCell.h"
#import <MWFeedParser/MWFeedParser.h>
#import <TPKeyboardAvoidingScrollView.h>


@interface LoginViewController : LMBaseViewController<MWFeedParserDelegate>

@property (nonatomic, assign) NSInteger currentLesson;
@property (nonatomic, assign) NSInteger maximumLessonNumber;

@property (weak, nonatomic) IBOutlet UIView *signupHolderView;
@property (weak, nonatomic) IBOutlet UIView *signinHolderView;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *signupEmailTextField;
@property (weak, nonatomic) IBOutlet UITextField *signupPasswordTextField;

@property (weak, nonatomic) IBOutlet UITextField *signupRepeatPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *signinEmailTextField;
@property (weak, nonatomic) IBOutlet UITextField *signinPasswordTextField;
@property (weak, nonatomic) IBOutlet UIImageView *mainBackgroundImageView;

@end

//
//  LoginViewController.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/7/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "LoginViewController.h"
#import "LMRequestManager.h"

@interface LoginViewController ()<CacheManagerDelegate> {
    
    __weak IBOutlet UILabel *userVipPassesLabel;
    __weak IBOutlet UIImageView *imageViewUserProfile;
    __strong ProgressUserObject *currentProgressUserObject;
    __strong ProgressLevelObject *currentProgressLevelObject;
    
    MWFeedParser *feedParser;
    NSMutableArray *parsedItems;
    
    __weak IBOutlet UIView *loadingView;
    __weak IBOutlet UIActivityIndicatorView *activityIndicatorLoading;
    __weak IBOutlet UILabel *loadingLabel;
    
    __weak IBOutlet UILabel *vipStaticTextLabel;
    __weak IBOutlet UILabel *signUpLabel;
    __weak IBOutlet UILabel *signInLabel;

    __weak IBOutlet UIButton *signUpSubmitButton;
    __weak IBOutlet UIButton *signInSubmitButton;
    __weak IBOutlet UILabel *orLabel;
    
}
@property (nonatomic, strong) NSMutableArray *lessonsInfoArray;

@end

@implementation LoginViewController


- (void)configureUI{
    
    [super configureUI];
    DLog(@"no_internet_access %@", Localized(@"no_internet_access"));
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    
    
    currentProgressLevelObject  = [DATA_MANAGER getCurrentProgressLevelObject];
//    maximumLessonNumber = currentProgressLevelObject.maxUnit;
    
    userVipPassesLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses];
    
    self.navigationController.navigationBarHidden = YES;
    
    /*imageViewUserProfile.layer.cornerRadius = 4.0;
    imageViewUserProfile.layer.borderWidth = 1.0;
    imageViewUserProfile.layer.borderColor = [UIColor blackColor].CGColor;
    imageViewUserProfile.layer.masksToBounds = YES;*/

    imageViewUserProfile.image = [USER_MANAGER takeCurrentUserImage];
    
    userVipPassesLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:15.0];
    vipStaticTextLabel.font = [UIFont fontWithName:@"PoiretOne-Regular" size:18.0];
    
    orLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"or_label"];
    
    signUpLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"sign_up_title"];
    _signupEmailTextField.placeholder = [LABEL_MANAGER setLocalizeLabelText:@"email_label"];
    _signupPasswordTextField.placeholder = [LABEL_MANAGER setLocalizeLabelText:@"password_label"];
    _signupRepeatPasswordTextField.placeholder = [LABEL_MANAGER setLocalizeLabelText:@"re_enter_password_label"];
    
    signInLabel.text = [LABEL_MANAGER setLocalizeLabelText:@"sign_in_title"];
    _signinEmailTextField.placeholder = [LABEL_MANAGER setLocalizeLabelText:@"email_label"];
    _signinPasswordTextField.placeholder = [LABEL_MANAGER setLocalizeLabelText:@"password_label"];
    
    [APPSTYLE applyTitle:[LABEL_MANAGER setLocalizeLabelText:@"submit_button_label"] toButton:signUpSubmitButton];
    [APPSTYLE applyTitle:[LABEL_MANAGER setLocalizeLabelText:@"submit_button_label"] toButton:signInSubmitButton];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self.view.layer removeAllAnimations];
    feedParser.delegate = nil;
    [feedParser stopParsing];
    feedParser = nil;
    [super viewDidDisappear:animated];
}


#pragma mark Orientations

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskPortrait;
    
}


#pragma mark - Open lesson

- (IBAction)openUserZone:(id)sender {
    
    [APP_DELEGATE buildUserZoneStack];
}

- (IBAction)openVipZone:(id)sender {
    
    [self.view bringSubviewToFront:loadingView];
    loadingView.hidden = NO;
    [activityIndicatorLoading startAnimating];
    
    //donwload resoureces
    NSArray *dataArray = [DATA_MANAGER getAllVipCategory];
    
    [CACHE_MANAGER setDelegate:self];
    [CACHE_MANAGER startDownloadingAndCachingResources:dataArray];


}


#pragma mark MWFeedParserDelegate

- (void)feedParserDidStart:(MWFeedParser *)parser {
    //NSLog(@"Started Parsing: %@", parser.url);
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedInfo:(MWFeedInfo *)info {
    //NSLog(@"Parsed Feed Info: “%@”", info.title);
    self.title = info.title;
}

- (void)feedParser:(MWFeedParser *)parser didParseFeedItem:(MWFeedItem *)item {
    //NSLog(@"Parsed Feed Item: “%@”", item.title);
    if (item) [parsedItems addObject:item];
}

#pragma mark - Sign In, Sign Up actions

- (IBAction)signInAction:(id)sender {
    
    if (![self isEmailFieldValid] || ![self isPasswordFieldValid]) {
        
        return;
    }
    
    [self addActivityIndicator];
    
    NSDictionary *dictHeaders = @{
                                  kUserEmail : _signinEmailTextField.text,
                                  kUserPassword : _signinPasswordTextField.text
                                  };
    [self addBaseActivityIndicator];
    [self chekInternet];
    [REQUEST_MANAGER getDataFor:kRequestLoginUserAPICall headers:dictHeaders withSuccessCallBack:^(NSDictionary *responseObject) {
        [self removeActivityIndicator];
        [USER_MANAGER initializeWithUser];
        
        [APP_DELEGATE buildHomePageStack];
        
        
    } andWithErrorCallBack:^(NSString *errorMessage) {
        
        [self removeActivityIndicator];
        
    }];
    
}

- (IBAction)signUpAction:(id)sender {
    
    
}

#pragma mark - Validations

- (BOOL)isEmailFieldValid {
    
    if (_signinEmailTextField.text.length == 0) {
        
        FCAlertView *alert = [ALERT_MANAGER setAlertViewWithTitle:@"Please enter your email" andSubstring:@""];
        
        [alert showAlertInView:self
                     withTitle:alert.title
                  withSubtitle:alert.subTitle
               withCustomImage:nil
           withDoneButtonTitle:nil
                    andButtons:nil];
        
        
        return NO;
    }
    if (![NSString validateEmail:_signinEmailTextField.text]) {
        
        FCAlertView *alert = [ALERT_MANAGER setAlertViewWithTitle:@"Please enter a valid email" andSubstring:@""];
        
        [alert showAlertInView:self
                     withTitle:alert.title
                  withSubtitle:alert.subTitle
               withCustomImage:nil
           withDoneButtonTitle:nil
                    andButtons:nil];
        
        return NO;
    }
    
    return YES;
}

- (BOOL)isPasswordFieldValid {
    
    if (_signinPasswordTextField.text.length == 0) {
     
        FCAlertView *alert = [ALERT_MANAGER setAlertViewWithTitle:@"Please enter a password" andSubstring:@""];

        [alert showAlertInView:self
                     withTitle: alert.title
                  withSubtitle: alert.subTitle
               withCustomImage:nil
           withDoneButtonTitle:nil
                    andButtons:nil];
        
        return NO;
    }
    
    if (_signinPasswordTextField.text.length < 4) {
     
        FCAlertView *alert = [ALERT_MANAGER setAlertViewWithTitle:@"" andSubstring:@"Your password must include at least 4 digits"];
        
        [alert showAlertInView:self
                     withTitle: alert.title
                  withSubtitle: alert.subTitle
               withCustomImage:nil
           withDoneButtonTitle:nil
                    andButtons:nil];

        return NO;
    }
    
    return YES;
}

@end

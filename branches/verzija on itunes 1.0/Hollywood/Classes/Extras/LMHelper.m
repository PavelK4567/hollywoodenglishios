//
//  LMHelper.m
//  MissingWord
//
//  Created by Darko Trpevski on 12/11/14.
//  Copyright (c) 2014 LaMark. All rights reserved.
//

#import "LMHelper.h"


@implementation LMHelper

#pragma mark - isWordMatchingPattern

+ (BOOL)isWordMatchingPattern:(NSString *)word
                      pattern:(NSString *)pattern
{
 
    NSPredicate *matchingExpression = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    
    if ([matchingExpression evaluateWithObject: word]) {
        return YES;
    }
    else {
        return NO;
    }
}


#pragma mark - getSubstringsThatMatchPattern

+ (NSArray *)getSubstringsThatMatchPattern:(NSString *)pattern
                            originalString:(NSString *)originalString
{
    NSMutableArray *arrayOfSubstring = [NSMutableArray new];
    
    NSError *error = nil;
    NSString *string = originalString;
    NSRange range = NSMakeRange(0, string.length);
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:&error];
    
    /** TODO:Refactor this part of code **/
    NSArray *matches = [regex matchesInString:string options:NSMatchingProgress range:range];
    
    
    for (NSTextCheckingResult *match in matches) {
        NSRange wordRange = [match rangeAtIndex:0];
        NSString *word = [string substringWithRange:wordRange];
        [arrayOfSubstring addObject:word];
    }
    
    return arrayOfSubstring;
}





#pragma mark - placeholdersWords

+ (NSArray *)placeholdersWords:(NSArray *)words {

    NSMutableArray *placeholderList = [NSMutableArray new];
    for (NSString *word in words) {
        
        if([LMHelper isWordMatchingPattern:word pattern:kWordPattern]) {
            [placeholderList addObject:kUnderlineWord];
        }
        else {
            [placeholderList addObject:word];
        }
            
    }
    return placeholderList;
}


#pragma mark - removeChar

+ (NSString *)removeChar:(NSString *)charString
              fromString:(NSString *)word
{
    return [word stringByReplacingOccurrencesOfString:charString withString:@""];
}

#pragma mark - replce special chars from string

+ (NSString *)removeSpecialChars:(NSString *)string
{
    NSString *modifiedString = string;
    modifiedString = [modifiedString stringByReplacingOccurrencesOfString:@"<" withString:@" "];
    modifiedString = [modifiedString stringByReplacingOccurrencesOfString:@">" withString:@" "];
    return modifiedString;
}



#pragma mark - removeCharsFromString

+ (NSString *)removeCharsFromString:(NSString *)word
{
    return [self removeChar:@"<" fromString:[self removeChar:@">" fromString:word]];
}


+ (NSMutableArray *)shuffleArray:(NSMutableArray *)array
{
    
    NSMutableArray *randomArray = [NSMutableArray arrayWithArray:array];
    
    NSInteger count = randomArray.count * 5;
    
    for (int i = 0; i < count; i++) {
        
        int index1 = i % [randomArray count];
        int index2 = arc4random() % [randomArray count];
        
        if (index1 != index2) {
            [randomArray exchangeObjectAtIndex:index1 withObjectAtIndex:index2];
        }
        
    }
    
    return randomArray;
}




#pragma mark - getUsersInfo

- (NSArray *)getUsersInfo
{
    NSData *userData          = [[NSUserDefaults standardUserDefaults] objectForKey:@"userinfo"];
    NSMutableArray *userArray = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
    
    return userArray;
}

#pragma mark - Validate email


+ (BOOL)isMailValid:(NSString *)email
{
    NSString *emailRegEx =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegEx];
    return [regExPredicate evaluateWithObject:[email lowercaseString]];
}



#pragma mark - saveModulCertificate


+ (void)saveModulCertificate:(NSInteger)modul
                       level:(NSInteger)level
                        user:(NSString *)user
{
 
    if(![self isModuleSaved:user modul:modul level:level]) {
       
        NSData *modulData          = [[NSUserDefaults standardUserDefaults] objectForKey:@"modulsList"];
        NSMutableArray *modulArray = [NSKeyedUnarchiver unarchiveObjectWithData:modulData];
        NSString *record           = [NSString stringWithFormat:@"%@#%ld#%ld",user,(long)modul,(long)level];
    
        if(!modulArray){
            modulArray = [[NSMutableArray alloc] init];
        }
        
        [modulArray addObject:record];
        
        NSData *statisticData = [NSKeyedArchiver archivedDataWithRootObject:modulArray];
        [[NSUserDefaults standardUserDefaults] setObject:statisticData forKey:@"modulsList"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}


#pragma mark - isModuleSaved

+ (BOOL)isModuleSaved:(NSString *)user
                modul:(NSInteger)modul
                level:(NSInteger)level
{
    
    NSData *modulData          = [[NSUserDefaults standardUserDefaults] objectForKey:@"modulsList"];
    NSMutableArray *modulArray = [NSKeyedUnarchiver unarchiveObjectWithData:modulData];
    
    for (NSString *modulString in modulArray) {
        
        NSString *checkString = [NSString stringWithFormat:@"%@#%ld#%ld",user,(long)modul,(long)level];
        if([modulString isEqualToString:checkString])
            return YES;
    }
    return NO;
}


#pragma mark - levelCompletition

+ (void)saveLevelCompletition:(NSString *)user
                         type:(NSInteger)type
                        level:(NSInteger)level

{
    
    if(![self isLevelCompletitionSaved:user type:type level:level]) {
        
        NSData *levelData          = [[NSUserDefaults standardUserDefaults] objectForKey:@"levelcompletition"];
        NSMutableArray *levelArray = [NSKeyedUnarchiver unarchiveObjectWithData:levelData];
        NSString *record           = [NSString stringWithFormat:@"%@#%ld#%ld",user,(long)type,(long)level];
        
        if(!levelArray){
            levelArray = [[NSMutableArray alloc] init];
        }
        
       [levelArray addObject:record];
        
        NSData *statisticData = [NSKeyedArchiver archivedDataWithRootObject:levelArray];
        [[NSUserDefaults standardUserDefaults] setObject:statisticData forKey:@"levelcompletition"];
        [[NSUserDefaults standardUserDefaults] synchronize];
   }
}


#pragma mark - isLevelCompletitionSaved

+ (BOOL)isLevelCompletitionSaved:(NSString *)user
                            type:(NSInteger)type
                           level:(NSInteger)level
{
    
    NSData *levelData          = [[NSUserDefaults standardUserDefaults] objectForKey:@"levelcompletition"];
    NSMutableArray *levelArray = [NSKeyedUnarchiver unarchiveObjectWithData:levelData];
    
    for (NSString *levelString in levelArray) {
        
        NSString *checkString = [NSString stringWithFormat:@"%@#%ld#%ld",user,(long)type,(long)level];
        if([levelString isEqualToString:checkString])
            return YES;
    }
    
    return NO;
}

#pragma mark - getNumberOfGestCredits





#pragma mark - getDeviceId

+ (NSString *)getDeviceId
{
    return [UIDevice currentDevice].identifierForVendor.UUIDString;
}

#pragma mark - Save to NSUserDefaults

+ (void)saveValue:(NSString *)value forKey:(NSString *)key
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if (standardUserDefaults) {
        [standardUserDefaults setObject:value forKey:key];
        [standardUserDefaults synchronize];
    }
}


+ (id)getValueForKey:(NSString *)key
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    
    if (standardUserDefaults) {
        return [standardUserDefaults objectForKey:key];
    }
    
    else return nil;
}


#pragma mark -
@end

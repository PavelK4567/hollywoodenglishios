//
//  MultipleChoiceText.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 4/7/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewVehicle.h"
#import "ClickTextAnswer.h"
#import "KSVideoPlayerView.h"
#import "VehicleObjectMultipleText+CoreDataClass.h"

@interface MultipleChoiceText : BaseViewVehicle<ClickTextAnswerDelegate, playerViewDelegate, LMAudioManagerDelegate>

@property (nonatomic, strong) id dataMultiChoiceTextInstance;
@property (nonatomic, strong) VehicleObjectMultipleText *dataVehicleObject;
@property (nonatomic, assign) BOOL isOrdered;

@property (nonatomic, strong) UIView *viewAnswerContent;

@property (nonatomic, strong) UIView *viewQuestionContent;
@property (nonatomic, strong) UIImageView *imageViewQuestion;

@property (nonatomic, assign) ClickTextAnswer *currentAnswer;
@property (assign, nonatomic) NSString *videoUrlString;
@property (nonatomic, strong) KSVideoPlayerView* videoPlayer;

- (instancetype)initWithFrame:(CGRect)frame data:(id)data isOrdered:(BOOL)type;

@property (nonatomic, assign) BOOL isAnsweredCorrect;


@end

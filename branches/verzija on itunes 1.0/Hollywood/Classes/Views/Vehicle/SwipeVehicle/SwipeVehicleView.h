//
//  SwipeVehicleView.h
//  NavigationHollywood
//
//  Created by Dimitar Shopovski on 3/21/16.
//  Copyright © 2016 Dimitar Shopovski. All rights reserved.
//

#import "ECVehicleDisplay.h"
#import "BaseViewVehicle.h"

#import "SwipeMovingView.h"
#import "SwipeDestinationView.h"


@interface SwipeVehicleView : BaseViewVehicle <SwipeMovingViewDelegate, BaseViewVehicleDelegate> {
    
    SwipeDestinationView *swipeDestinationViewLeft;
    SwipeDestinationView *swipeDestinationViewRight;
    
    SwipeMovingView *swipeMovingView;
    
}

@property (nonatomic, strong) id vehicleData;

- (instancetype)initWithFrame:(CGRect)frame data:(id)data;

@end

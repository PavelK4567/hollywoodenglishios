//
//  VoiceRecoginitionVehicle.h
//  Hollywood
//
//  Created by Kiril Kiroski on 4/18/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "BaseViewVehicle.h"
#import "VehicleObjectVoiceRecognition+CoreDataClass.h"


@interface VoiceRecoginitionVehicle : BaseViewVehicle <LMAudioManagerDelegate>

@property (nonatomic, strong) VehicleObjectVoiceRecognition *dataVehicleObject;

- (instancetype)initWithFrame:(CGRect)frame data:(id)data;


@end

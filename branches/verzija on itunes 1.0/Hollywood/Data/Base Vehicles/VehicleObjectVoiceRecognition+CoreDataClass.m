//
//  VehicleObjectVoiceRecognition+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectVoiceRecognition+CoreDataClass.h"

@implementation VehicleObjectVoiceRecognition

- (void) setData:(NSDictionary*)dataDictionary{
    [super setData:dataDictionary];
    //DLog(@"## %@",dataDictionary);
    
    if( [dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.mainImageId = [self objectOrNilForKey:kVehicleObjectMainImageId fromDictionary:dataDictionary];
        self.targetAudio = [self objectOrNilForKey:kVehicleObjectTargetAudio fromDictionary:dataDictionary];
        self.targetText = [self objectOrNilForKey:kVehicleObjectTargetText fromDictionary:dataDictionary];
    }
    
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end

//
//  InstanceAnswersSwipeObject+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceAnswersSwipeObject+CoreDataClass.h"

@implementation InstanceAnswersSwipeObject

- (void) setData:(NSDictionary*)dataDictionary{
    [super setData:dataDictionary];
    //DLog(@"## %@",dataDictionary);
    
    
    if( [dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.image = [self objectOrNilForKey:kInstanceObjectImage fromDictionary:dataDictionary];
        self.free = [NSNumber numberWithInteger:[[self objectOrNilForKey:kFree fromDictionary:dataDictionary]integerValue]];
        self.isCorrect = [NSNumber numberWithInteger:[[self objectOrNilForKey:kInstanceObjectIsCorrect fromDictionary:dataDictionary]integerValue]];
        self.itemPostion = [NSNumber numberWithInteger:[[self objectOrNilForKey:kInstanceObjectItemPostion fromDictionary:dataDictionary]integerValue]];
        self.text = [self objectOrNilForKey:kInstanceObjectText fromDictionary:dataDictionary];
    }
    
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end

//
//  InstanceAnswersSwipeObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 7/6/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceAnswersSwipeObject+CoreDataProperties.h"

@implementation InstanceAnswersSwipeObject (CoreDataProperties)

+ (NSFetchRequest<InstanceAnswersSwipeObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"InstanceAnswersSwipeObject"];
}

@dynamic free;
@dynamic image;
@dynamic isCorrect;
@dynamic itemPostion;
@dynamic text;

@end

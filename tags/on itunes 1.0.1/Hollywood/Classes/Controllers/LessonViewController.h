//
//  LessonViewController.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/24/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewVehicle.h"
#import "SwipeKircaVehicle.h"
#import "SwipeCarouselVehicle.h"
#import "KSVideoPlayerView.h"
#import "LessonProgressView.h"
#import "VoiceRecoginitionVehicle.h"
#import "CacheManager.h"
#import "LMBaseViewController.h"

#import "VehicleObjectVideo+CoreDataClass.h"
#import "VehicleObjectVoiceRecognition+CoreDataClass.h"
#import "VehicleObjectSwipeCarousel+CoreDataClass.h"
#import "VehicleObjectMultipleText+CoreDataClass.h"
#import "VehicleObjectMultipleImage+CoreDataClass.h"
#import "VehicleObjectFillTheMissing+CoreDataClass.h"
#import "VehicleObjectClickToLearn+CoreDataClass.h"
#import "VehicleObjectSwipe+CoreDataClass.h"
#import "VehicleObjectSpelling+CoreDataClass.h"

@interface LessonViewController : LMBaseViewController <CacheManagerDelegate, UIScrollViewDelegate, BaseViewVehicleDelegate,playerViewDelegate, UIGestureRecognizerDelegate> {
    
    NSInteger lastPage, currentPage;
    BOOL isBackFromConversion;
}

@property (nonatomic, assign) NSInteger lessonNumber;
@property (nonatomic, assign) BOOL continueSession;
@property (nonatomic, assign) BOOL isLoadingFinished;
@property (nonatomic, assign) BOOL isPopupOpened;
@property (nonatomic, assign) BOOL videoHihgQuality;

@property (weak, nonatomic) IBOutlet UILabel *execiseChoiseLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (nonatomic) float vehicleHeight;
@property (nonatomic) float vehicleWidth;

@end

//
//  SettingsDiplomasCollectionViewCell.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/25/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "SettingsDiplomasCollectionViewCell.h"
#import <GPUImage/GPUImageSaturationFilter.h>
#import <GPUImage/GPUImagePicture.h>

@implementation SettingsDiplomasCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)getImageFromURL:(NSURL *)url withCompletionHandler:(ResponseBlock)completionBlock
{
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
    
    if(image != nil)
    {
        completionBlock(image, nil);
    }
    else
    {
        NSError *error;
        completionBlock(nil, error);
    }
}

- (BOOL)checkMaxRankForRow:(NSInteger)row
{
    if(row <= ([[USER_MANAGER takeMaxRank] intValue] - 1))
    {
        return YES;
    }
    return NO;
}

- (UIImage *)makeSaturationForImage:(UIImage *)image
{
    GPUImagePicture *stillImageSource = [[GPUImagePicture alloc] initWithImage: image];
    GPUImageSaturationFilter *saturationFilter = [GPUImageSaturationFilter new];
    saturationFilter.saturation = 0.20;
    [stillImageSource addTarget:saturationFilter];
    [saturationFilter useNextFrameForImageCapture];
    [stillImageSource processImage];
    
    UIImage *currentFilteredVideoFrame =  [saturationFilter imageFromCurrentFramebufferWithOrientation:UIImageOrientationUp];
    
    return currentFilteredVideoFrame;
}

@end

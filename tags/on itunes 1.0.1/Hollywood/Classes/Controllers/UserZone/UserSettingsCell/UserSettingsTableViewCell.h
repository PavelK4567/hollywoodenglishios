//
//  UserSettingsTableViewCell.h
//  Hollywood
//
//  Created by Dimitar Shopovski on 8/10/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserSettingsTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, weak) IBOutlet UILabel *optionNameLabel;

@end

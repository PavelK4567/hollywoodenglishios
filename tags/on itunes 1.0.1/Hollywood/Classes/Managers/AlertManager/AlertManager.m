//
//  LMAlertManager.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 8/7/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "AlertManager.h"

@implementation AlertManager

#ifdef ALERTMANAGER_SINGLETON


SINGLETON_GCD(AlertManager)
#endif



- (void)setAlertViewWith:(NSString *)title and: (NSString *)subtitle
{
    FCAlertView *alertView = [[FCAlertView alloc] init];
    alertView.alertBackgroundColor = [UIColor colorWithRed:112.0f/255.0f green:102.0f/255.0f blue:55.0f/255.0f alpha:1.0];
    alertView.backgroundColor = [UIColor clearColor];
    alertView.colorScheme = [UIColor blackColor];
    alertView.doneButtonTitleColor = [UIColor whiteColor];
    alertView.hideSeparatorLineView = YES;
    alertView.blurBackground = NO;
    
    [alertView setSubTitle:subtitle];
    [alertView setTitle:title];
    
}


- (FCAlertView *)setAlertViewWithTitle:(NSString *)title andSubstring: (NSString *)subtitle
{
    FCAlertView *alertView = [[FCAlertView alloc] init];
    alertView.alertBackgroundColor = [UIColor colorWithRed:112.0f/255.0f green:102.0f/255.0f blue:55.0f/255.0f alpha:1.0];
    alertView.backgroundColor = [UIColor clearColor];
    alertView.colorScheme = [UIColor blackColor];
    alertView.doneButtonTitleColor = [UIColor whiteColor];
    alertView.hideSeparatorLineView = YES;
    alertView.blurBackground = NO;
    
    [alertView setSubTitle:subtitle];
    [alertView setTitle:title];
    
    return alertView;
}


- (void)showCustomAlertWithTitle:(NSString *)title andMessage:(NSString *)message {
    
    FCAlertView *alertView = [[FCAlertView alloc] init];
    alertView.alertBackgroundColor = [UIColor colorWithRed:112.0f/255.0f green:102.0f/255.0f blue:55.0f/255.0f alpha:1.0];
    alertView.backgroundColor = [UIColor clearColor];
    alertView.colorScheme = [UIColor blackColor];
    alertView.doneButtonTitleColor = [UIColor whiteColor];
    alertView.hideSeparatorLineView = YES;
    alertView.blurBackground = NO;
    
    [alertView setSubTitle:message];
    [alertView setTitle:title];
    
    
    [alertView showAlertWithTitle:title withSubtitle:message withCustomImage:nil withDoneButtonTitle:@"Ok" andButtons:nil];

}




@end

//
//  JSONManager.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 9/22/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "JSONManager.h"

@implementation JSONManager

- (id)init
{
    self = [super init];
    if (self) {
        
        //init
        
    }
    return self;
}

+ (id)sharedInstance {
    
    static JSONManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
    
}

#pragma mark - Get local json file
- (void)getLocalJSONFile:(NSString *)fileName {
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:fileName ofType:@"json"];
    NSData* data = [NSData dataWithContentsOfFile:filePath];
    if (!data) {
        return;
    }
    
    __autoreleasing NSError* error = nil;
    id result = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions error:&error];

    if (error != nil) {
        
        NSLog(@"Error");
    }
    else {
        
        //Parse
        
        NSLog(@"JSON - %@", result);
    }
    
}

@end

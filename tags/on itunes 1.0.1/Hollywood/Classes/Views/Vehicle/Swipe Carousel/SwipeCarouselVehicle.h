//
//  SwipeCarouselVehicle.h
//  Hollywood
//
//  Created by Kiril Kiroski on 4/1/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "BaseViewVehicle.h"
#import "VehicleObjectSwipeCarousel+CoreDataClass.h"

@interface SwipeCarouselVehicle : BaseViewVehicle<UIPickerViewDataSource,UIPickerViewDelegate,UIGestureRecognizerDelegate>

@property (nonatomic, strong) id vehicleData;
@property (nonatomic, strong) VehicleObjectSwipeCarousel *dataVehicleObject;

- (instancetype)initWithFrame:(CGRect)frame data:(id)data;


@end

//
//  AchievementsObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/11/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "AchievementsObject+CoreDataProperties.h"

@implementation AchievementsObject (CoreDataProperties)

+ (NSFetchRequest<AchievementsObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"AchievementsObject"];
}

@dynamic achievement_id;
@dynamic achievementType;
@dynamic image;
@dynamic afterImage;
@dynamic achivmentText;
@dynamic teaserText;
@dynamic award;
@dynamic criteria;

@end

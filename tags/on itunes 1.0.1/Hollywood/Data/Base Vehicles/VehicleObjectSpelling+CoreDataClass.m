//
//  VehicleObjectSpelling+CoreDataClass.m
//  Hollywood
//
//  Created by Aleksandar Jovanov on 6/30/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectSpelling+CoreDataClass.h"

@implementation VehicleObjectSpelling

- (void) setData:(NSDictionary*)dataDictionary{
    [super setData:dataDictionary];
    //DLog(@"## %@",dataDictionary);
    
    
    if( [dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.sentence = [self objectOrNilForKey:kVehicleObjectSentence fromDictionary:dataDictionary];
    }
    
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end

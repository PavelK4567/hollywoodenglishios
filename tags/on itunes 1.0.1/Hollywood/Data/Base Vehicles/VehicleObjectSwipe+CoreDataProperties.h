//
//  VehicleObjectSwipe+CoreDataProperties.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 6/30/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectSwipe+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface VehicleObjectSwipe (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectSwipe *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *initial;
@property (nullable, nonatomic, copy) NSString *lesson_description;
@property (nullable, nonatomic, copy) NSString *lesson_home_page_image;
@property (nullable, nonatomic, copy) NSNumber *lesson_id;
@property (nullable, nonatomic, copy) NSString *lesson_name;
@property (nullable, nonatomic, copy) NSString *lesson_teaser;
@property (nullable, nonatomic, retain) NSSet<InstanceAnswersSwipeObject *> *instanceAnswers;

@end

@interface VehicleObjectSwipe (CoreDataGeneratedAccessors)

- (void)addInstanceAnswersObject:(InstanceAnswersSwipeObject *)value;
- (void)removeInstanceAnswersObject:(InstanceAnswersSwipeObject *)value;
- (void)addInstanceAnswers:(NSSet<InstanceAnswersSwipeObject *> *)values;
- (void)removeInstanceAnswers:(NSSet<InstanceAnswersSwipeObject *> *)values;

@end

NS_ASSUME_NONNULL_END

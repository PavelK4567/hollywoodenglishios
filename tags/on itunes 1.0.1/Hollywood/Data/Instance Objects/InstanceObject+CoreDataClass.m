//
//  InstanceObject+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceObject+CoreDataClass.h"

@implementation InstanceObject

- (void) setData:(NSDictionary*)dataDictionary{
    //DLog(@"## %@",dataDictionary);
    //self.instanceObject_id = [self objectOrNilForKey:kCourseObjectCourseId fromDictionary:dataDictionary];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}
@end

//
//  InstanceObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceObject+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface InstanceObject (CoreDataProperties)

+ (NSFetchRequest<InstanceObject *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *instanceObject_id;

@end

NS_ASSUME_NONNULL_END

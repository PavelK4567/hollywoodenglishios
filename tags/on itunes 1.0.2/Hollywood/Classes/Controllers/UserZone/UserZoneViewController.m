//
//  UserZoneViewController.m
//  Hollywood
//
//  Created by Dimitar Shopovski on 8/10/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "UserZoneViewController.h"
#import "UserSettingsCell/UserSettingsTableViewCell.h"

@interface UserZoneViewController () {
    
    __weak IBOutlet UILabel *userScoreLabel;
    __weak IBOutlet UILabel *userVipPassesLabel;
    
    __strong ProgressUserObject *currentProgressUserObject;
    __strong ProgressLevelObject *currentProgressLevelObject;

    __weak IBOutlet UIImageView *imageViewUserProfile;
    __weak IBOutlet UIImageView *imageViewUserRank;

}

@property (nonatomic, weak) IBOutlet UITableView *userOptionsTableView;
@property (nonatomic, strong) NSMutableArray *userOptionsArray;

@end

@implementation UserZoneViewController

- (void)loadData {
    [super loadData];

    
    self.userOptionsArray = [[NSMutableArray alloc] initWithObjects:@{
                                                                      @"optionName": [LABEL_MANAGER setLocalizeLabelText:@"settings_title"],
                                                                      @"optionImage": @"settings-option",
                                                                      
                                                                      },
                             @{
                               @"optionName": [LABEL_MANAGER setLocalizeLabelText:@"progress_title"],
                               @"optionImage": @"progress-option",
                               
                               },
                             @{
                               @"optionName": [LABEL_MANAGER setLocalizeLabelText:@"diplomas_title"],
                               @"optionImage": @"diplomas-option",
                               
                               },
                             @{
                               @"optionName": [LABEL_MANAGER setLocalizeLabelText:@"challenges_title"],
                               @"optionImage": @"challenges-option",
                               
                               },
                             @{
                               @"optionName": [LABEL_MANAGER setLocalizeLabelText:@"vocab_title"],
                               @"optionImage": @"vocab-option",
                               
                               }, nil];
}

- (void)configureUI{
    
    [super configureUI];
    self.navigationController.navigationBarHidden = YES;

    [self.userOptionsTableView registerNib:[UINib nibWithNibName:@"UserSettingsTableViewCell" bundle:nil] forCellReuseIdentifier:@"CellUserSettings"];

    /*imageViewUserProfile.layer.cornerRadius = 4.0;
    imageViewUserProfile.layer.borderWidth = 1.0;
    imageViewUserProfile.layer.borderColor = [UIColor blackColor].CGColor;
    imageViewUserProfile.layer.masksToBounds = YES;*/
    
    imageViewUserProfile.image = [USER_MANAGER takeCurrentUserImage];
    
    currentProgressUserObject = [DATA_MANAGER getCurrentUserProgressObject];
    currentProgressLevelObject  = [DATA_MANAGER getCurrentProgressLevelObject];
    
    userScoreLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.score];
    userVipPassesLabel.text = [NSString stringWithFormat:@"%d", currentProgressUserObject.vipPasses];
    
    imageViewUserRank.image = [UIImage imageNamed:[NSString stringWithFormat:@"Awards_%02d", [[USER_MANAGER takeMaxRank] intValue] + 1]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Back to home page

- (IBAction)goToHomePage:(id)sender {
    
    [APP_DELEGATE buildHomePageStack];
}


#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.userOptionsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    

    UserSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CellUserSettings"];
    
    id option = [self.userOptionsArray objectAtIndex:indexPath.row];
    [cell.optionNameLabel setText:[option objectForKey:@"optionName"]];
    
    [cell.backgroundImageView setImage:[UIImage imageNamed:[option objectForKey:@"optionImage"]]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 105.0;
}

#pragma mark Orientations
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskPortrait;
    
}

@end

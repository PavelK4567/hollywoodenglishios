//
//  VehicleObjectFillTheMissing+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectFillTheMissing+CoreDataProperties.h"

@implementation VehicleObjectFillTheMissing (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectFillTheMissing *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"VehicleObjectFillTheMissing"];
}

@dynamic fillWholeSentence;
@dynamic sentence;
@dynamic vehicleWords;

@end

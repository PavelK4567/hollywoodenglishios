//
//  VehicleObjectSwipeCarousel+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 7/4/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectSwipeCarousel+CoreDataClass.h"
#import "InstanceAnswersSwipeCarouselObject+CoreDataClass.h"

@implementation VehicleObjectSwipeCarousel

- (void) setData:(NSDictionary*)dataDictionary{
    [super setData:dataDictionary];
    //DLog(@"## %@",dataDictionary);
    
    
    NSObject *receivedLessonsVehicles = [dataDictionary objectForKey:kVehicleObjectInstanceAnswer];
    if ([receivedLessonsVehicles isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedLessonsVehicles) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                InstanceAnswersSwipeCarouselObject *importedObject = [InstanceAnswersSwipeCarouselObject MR_createEntity];
                
                [importedObject setData:item];
                [self addInstanceAnswersObject:importedObject];
            }
        }
    }
}
@end

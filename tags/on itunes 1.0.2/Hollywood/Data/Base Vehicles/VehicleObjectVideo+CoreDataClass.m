//
//  VehicleObjectVideo+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectVideo+CoreDataClass.h"

@implementation VehicleObjectVideo

- (void) setData:(NSDictionary*)dataDictionary{
    [super setData:dataDictionary];
    //DLog(@"## %@",dataDictionary);
    
    if( [dataDictionary isKindOfClass:[NSDictionary class]]) {
        

        self.textInfo = [self objectOrNilForKey:kVehicleObjectVideoTextInfo fromDictionary:dataDictionary];
        self.videoPreview = [self objectOrNilForKey:kVehicleObjectVideoPreview fromDictionary:dataDictionary];
        self.videoType = [self objectOrNilForKey:kVehicleObjectVideoType fromDictionary:dataDictionary];
        self.freeVideoLength = [self objectOrNilForKey: kVehicleObjectFreeVideoLength fromDictionary: dataDictionary];
        NSObject *receivedVideoVehicles = [dataDictionary objectForKey:kVehicleObjectVideo];
        if ([receivedVideoVehicles isKindOfClass:[NSArray class]]) {
            NSDictionary *items = [receivedVideoVehicles mutableCopy];
            self.videoLowQuality = [items valueForKey:@"lowQuality"];
            self.videoHighQuality = [items valueForKey:@"highQuality"];
        }else if([receivedVideoVehicles isKindOfClass:[NSDictionary class]]){
            self.videoLowQuality = [receivedVideoVehicles valueForKey:@"lowQuality"];
            self.videoHighQuality = [receivedVideoVehicles valueForKey:@"highQuality"];
        }
        NSLog(@"### self.videoLowQuality %@",self.videoLowQuality);
        NSLog(@"### self.videoHighQuality %@",self.videoHighQuality);
        NSLog(@"### ");
        if(!(self.videoLowQuality && self.videoHighQuality)){
            DLog(@"#    !!!WRONG!!!    # %@",dataDictionary);
        }
    }
    
    
    
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end

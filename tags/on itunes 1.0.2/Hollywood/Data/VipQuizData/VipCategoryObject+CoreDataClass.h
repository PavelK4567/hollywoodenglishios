//
//  VipCategoryObject+CoreDataClass.h
//  Hollywood
//
//  Created by Kiril Kiroski on 10/3/16.
//  Copyright © 2016 aa. All rights reserved.
//
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class VipQuizObject;

NS_ASSUME_NONNULL_BEGIN

@interface VipCategoryObject : NSManagedObject

- (void) setData:(NSDictionary*)dataDictionary;

@end

NS_ASSUME_NONNULL_END

#import "VipCategoryObject+CoreDataProperties.h"

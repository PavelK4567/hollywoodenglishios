//
//  SettingsViewController.h
//  Hollywood
//
//  Created by Aleksandar Jovanov on 7/26/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : LMBaseViewController

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *userProfileImageView;
@property (weak, nonatomic) IBOutlet UILabel *userScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *userVipPassesLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewUserRank;

@property (nonatomic, strong) NSMutableArray *userOptionsArray;

@end

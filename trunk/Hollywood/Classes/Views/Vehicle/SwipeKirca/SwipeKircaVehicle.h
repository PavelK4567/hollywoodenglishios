//
//  SwipeKircaVehicle.h
//  Hollywood
//
//  Created by Kiril Kiroski on 3/31/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "BaseViewVehicle.h"
#import "ECVehicleDisplay.h"
#import "VehicleObjectSwipe+CoreDataClass.h"
#import "SwipeMovingView.h"
#import "SwipeDestinationView.h"

#define DRAG_AREA_PADDING 5

@interface SwipeKircaVehicle : BaseViewVehicle <SwipeMovingViewDelegate, BaseViewVehicleDelegate,LMAudioManagerDelegate>{
    
    SwipeDestinationView *swipeDestinationViewLeft;
    SwipeDestinationView *swipeDestinationViewRight;
    
    SwipeMovingView *swipeMovingView;
    UIPanGestureRecognizer *panGesture;
    UIView *swipeHolderView;
    
}

@property (nonatomic, strong) id vehicleData;
@property (weak, nonatomic) NSLayoutConstraint *dragViewX;
@property (weak, nonatomic) NSLayoutConstraint *dragViewY;
@property (nonatomic, assign) BOOL swipeImageVehicle;
@property (nonatomic, strong) VehicleObjectSwipe *dataVehicleObject;

- (instancetype)initWithFrame:(CGRect)frame data:(id)data;
- (void)setVehicleType;

@end

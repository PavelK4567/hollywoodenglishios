//
//  VehicleObjectFillTheMissing+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "VehicleObjectFillTheMissing+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface VehicleObjectFillTheMissing (CoreDataProperties)

+ (NSFetchRequest<VehicleObjectFillTheMissing *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *fillWholeSentence;
@property (nullable, nonatomic, copy) NSString *sentence;
@property (nullable, nonatomic, retain) NSSet<InstanceAnswersWordsObject *> *vehicleWords;

@end

@interface VehicleObjectFillTheMissing (CoreDataGeneratedAccessors)

- (void)addVehicleWordsObject:(InstanceAnswersWordsObject *)value;
- (void)removeVehicleWordsObject:(InstanceAnswersWordsObject *)value;
- (void)addVehicleWords:(NSSet<InstanceAnswersWordsObject *> *)values;
- (void)removeVehicleWords:(NSSet<InstanceAnswersWordsObject *> *)values;

@end

NS_ASSUME_NONNULL_END

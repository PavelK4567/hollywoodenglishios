//
//  InstanceAnswersImageObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceAnswersImageObject+CoreDataProperties.h"

@implementation InstanceAnswersImageObject (CoreDataProperties)

+ (NSFetchRequest<InstanceAnswersImageObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"InstanceAnswersImageObject"];
}

@dynamic afterAudio;
@dynamic defaultImage;
@dynamic free;
@dynamic sourceLanguageText;
@dynamic targetLanguageText;

@end

//
//  InstanceAnswersWordsObject+CoreDataClass.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceAnswersWordsObject+CoreDataClass.h"

@implementation InstanceAnswersWordsObject

- (void) setData:(NSDictionary*)dataDictionary{
    [super setData:dataDictionary];
    //DLog(@"## %@",dataDictionary);
    
    if( [dataDictionary isKindOfClass:[NSDictionary class]]) {
        self.word = [self objectOrNilForKey:kWord fromDictionary:dataDictionary];
        self.audio = [self objectOrNilForKey:kAudio fromDictionary:dataDictionary];
        self.free = [NSNumber numberWithInteger:[[self objectOrNilForKey:kFree fromDictionary:dataDictionary]integerValue]];
    }
    
    
}


#pragma mark - Helper Method

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end

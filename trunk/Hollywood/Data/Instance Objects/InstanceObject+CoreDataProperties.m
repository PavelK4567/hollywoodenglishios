//
//  InstanceObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 6/27/17.
//  Copyright © 2017 aa. All rights reserved.
//

#import "InstanceObject+CoreDataProperties.h"

@implementation InstanceObject (CoreDataProperties)

+ (NSFetchRequest<InstanceObject *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"InstanceObject"];
}

@dynamic instanceObject_id;

@end

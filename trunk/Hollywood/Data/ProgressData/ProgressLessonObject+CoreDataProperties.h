//
//  ProgressLessonObject+CoreDataProperties.h
//  Hollywood
//
//  Created by Kiril Kiroski on 8/11/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ProgressLessonObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ProgressLessonObject (CoreDataProperties)

@property (nonatomic) int32_t maxVehicle;
@property (nonatomic) int32_t currentVehicle;
@property (nonatomic) int32_t scoreForLesson;
@property (nonatomic) int32_t starsNumber;
@property (nonatomic) int32_t lessonOrder;
@property (nullable, nonatomic, retain) NSSet<ProgressVehicleObject *> *vehicleProgres;

@end

@interface ProgressLessonObject (CoreDataGeneratedAccessors)

- (void)addVehicleProgresObject:(ProgressVehicleObject *)value;
- (void)removeVehicleProgresObject:(ProgressVehicleObject *)value;
- (void)addVehicleProgres:(NSSet<ProgressVehicleObject *> *)values;
- (void)removeVehicleProgres:(NSSet<ProgressVehicleObject *> *)values;

@end

NS_ASSUME_NONNULL_END

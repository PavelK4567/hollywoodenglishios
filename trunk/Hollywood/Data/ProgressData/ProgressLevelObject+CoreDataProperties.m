//
//  ProgressLevelObject+CoreDataProperties.m
//  Hollywood
//
//  Created by Kiril Kiroski on 8/11/16.
//  Copyright © 2016 aa. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ProgressLevelObject+CoreDataProperties.h"

@implementation ProgressLevelObject (CoreDataProperties)

@dynamic maxUnit;
@dynamic currentUnit;
@dynamic scoreForLevel;
@dynamic isOpen;
@dynamic diploma;
@dynamic levelOrder;
@dynamic lessonsProgres;

@end

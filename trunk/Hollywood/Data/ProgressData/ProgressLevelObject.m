//
//  ProgressLevelObject.m
//  Hollywood
//
//  Created by Kiril Kiroski on 8/10/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import "ProgressLevelObject.h"
#import "ProgressLessonObject.h"

@implementation ProgressLevelObject

- (void) setData:(NSDictionary*)dataDictionary
{
    
    
}


- (void) setInitialData;
{
    NSLog(@"ProgressLevelObject >>> setInitialData");
    self.maxUnit = 0;
    self.currentUnit = 0;
    self.scoreForLevel = 0;
    self.isOpen = NO;
    self.diploma = NO;
    NSArray *allLessonObject = [DATA_MANAGER takeAllLessonObject];
    int countForLessons = (int)[allLessonObject count]+1;
    for (int i= 0; i < countForLessons; i++) {
        ProgressLessonObject *newObject = [ProgressLessonObject MR_createEntity];
        newObject.lessonOrder = i;
        [newObject setInitialData];
        [self addLessonsProgresObject:newObject];
    }
}

@end

//
//  QuizInstance+CoreDataClass.h
//  Hollywood
//
//  Created by Kiril Kiroski on 10/28/16.
//  Copyright © 2016 aa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "QuizInstanceAnswer+CoreDataProperties.h"

NS_ASSUME_NONNULL_BEGIN

@interface QuizInstance : NSManagedObject

- (void) setData:(NSDictionary*)dataDictionary;

@end

NS_ASSUME_NONNULL_END

#import "QuizInstance+CoreDataProperties.h"
